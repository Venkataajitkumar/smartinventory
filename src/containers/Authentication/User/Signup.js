import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import * as actions from '../../../store/actions/index';


class Signup extends Component {

    state = {
        controls: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Full name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'name'
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                valueType: 'eMail'
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                valueType: 'password'
            },
            confirmPassword: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Confirm Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                valueType: 'password'
            },
            addressLineOne: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Address line 1'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'address'
            },
            addressLineTwo: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Apt, suit, etc'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'address'
            },
            city: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'City'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'city'
            },
            state: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'State'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'state'
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ZIP code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5,
                    isNumeric: true
                },
                valid: false,
                touched: false,
                valueType: 'ZIP code'
            },
        },
        formIsValid: false
    }

    onUserSignUp = () => {
        const isSignup = true
        let userData = {
            name: this.state.controls.name.value,
            email: this.state.controls.email.value,
            addressLineOne: this.state.controls.addressLineOne.value,
            addressLineTwo: this.state.controls.addressLineTwo.value,
            city: this.state.controls.city.value,
            state: this.state.controls.state.value,
            zipCode: this.state.controls.zipCode.value
        }
        this.props.onUserSignnedUp(this.state.controls.email.value, this.state.controls.password.value, isSignup, false, userData);
        this.props.history.push('/');
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }

        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ controls: updatedControls, formIsValid: formIsValid })
    }

    render() {

        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = ( 
            <form 
                onSubmit = {this.onUserSignUp}
                style ={{
                    width: "50%",
                    background: "white",
                    border: "1px solid #ccc",
                    margin: "auto",
                    marginTop: "30px",
                    padding: "10px",
                    borderRadius: "10px"
                }}>
                {
                    formElementArray.map( formElement => (
                        <Input 
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))
                }
                <Button 
                    style = {{
                        width: "100%",
                        margin: "auto" 
                    }} 
                    disabled = {!this.state.formIsValid}> SIGNUP </Button> 
            </form>
        );

        return form
    }
}

const mapStateToDispatch = state => {
    return {
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserSignnedUp: (email, password, isSignup, isAdmin, userData) => dispatch(actions.auth(email, password, isSignup, isAdmin, userData))
    }
}

export default connect(mapStateToDispatch, mapDispatchToProps)(withRouter(Signup));