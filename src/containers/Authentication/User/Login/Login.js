import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Input from '../../../../components/UI/Input/Input';
import Button from '../../../../components/UI/Button/Button';
import classes from './Login.module.css';
import * as actions from '../../../../store/actions/index';

class Login extends Component {

    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Mail address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                valueType: 'eMail'
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                valueType: 'password'
            }
        },
        formIsValid: false
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }
        
        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ controls: updatedControls, formIsValid: formIsValid })
    }

    onUserLogIn = (event) => {
        event.preventDefault();
        this.props.onUserLoggedIn(this.state.controls.email.value, this.state.controls.password.value, false, false);
        this.props.history.push("/userHome");
    }

    render() {

        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = ( 
            <form onSubmit = {(event) => this.onUserLogIn(event)}>
                {
                    formElementArray.map( formElement => (
                        <Input 
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))
                }
                <Button 
                    style = {{
                        width: "100%",
                        margin: "auto" 
                    }} 
                    disabled = {!this.state.formIsValid}> LOGIN </Button> 
            </form>
        );

        let errorMessage = null;

        if(this.props.error) {
            errorMessage = <p style = {{fontSize: "1rem", fontWeight: "300"}} >{this.props.error.message}</p>
        }

        return (
            <div>   
                {errorMessage}
                {form}
                <div className = {classes.Login}>
                    <div>Don't have an account</div>
                    <Link to="/signup" onClick = {this.props.clicked}>SIGNUP</Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null,
        error: state.auth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserLoggedIn: (email, password, isSignup, isAdmin) => dispatch(actions.auth(email, password, isSignup, isAdmin))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));