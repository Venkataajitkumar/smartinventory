import React, { Component } from 'react';

import classes from './Home.module.css';
import Aux from '../../../hoc/Auxiliary/Auxiliary';

class Home extends Component {
    render() {
        return (
            <Aux>
                <div className = {classes.Body}>
                    <section className={classes.showcase}></section>

                    <section className={classes.boxes}>
                        <div className={classes.container}>
                            <div className={classes.box}>
                                <h1>Admin</h1>
                                <p>
                                    In this Web Application <br />
                                    An Admin can add, edit and modify the product. An admin can also do
                                    accept or decline the user claim and also send the shiplabel and
                                    finally do make the payment
                                    <br />
                                    <br />
                                    <br />
                                </p>
                            </div>

                            <div className={classes.box}>
                                <h1>User</h1>
                                <p>
                                    In this Web Application <br />
                                    Once the user signup and login to inventory system he/she could able
                                    to view the post which are posted by admin. A user can claim a
                                    post,once the admin accepts user claim he/she recieves the shipment
                                    label and then the user will send the product and make a request for
                                    payment.
                                </p>
                            </div>

                            <div className={classes.box}>
                                <h1>About</h1>
                                <p>
                                    This inventory system will help a businessman manage his online
                                    store. This application will help both admin and user to buy and
                                    sell their products through online.<br />
                                    <br />
                                    <br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </Aux>
        );
    }
}

export default Home;