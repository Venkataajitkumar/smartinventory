import React, { Component } from 'react';
import { connect } from 'react-redux';

import UserModified from './UserModified/UserModified';
import classes from './UserHistory.module.css';
import * as actions from '../../../store/actions/index';

class UserHistory extends Component {

    state = {
        productsToDisplay: []
    }

    componentDidMount () {
        this.props.onInitData();
        this.props.onUserFetch();
        let products = [];
        for(let userData of this.props.userProducts) {
            for (let actualProduct of this.props.products) {
                if(userData.productID === actualProduct.id && userData.userId === this.props.userId) {
                    products.push({
                        productDetails: actualProduct,
                        numberOfClaims: userData.claimedProduct,
                        id: userData.id,
                        previousData: userData,
                        productId: userData.productID
                    });
                    break;
                }
            }
        }
        this.setState({ productsToDisplay: products })
    }

    render() {
        let products = null;
        if(this.state.productsToDisplay.length !== 0) {
            products = this.state.productsToDisplay.map(product => {
                let display = null;
                if(product.previousData.adminMode) {
                    display = (
                        <p>Your product has been <strong>{product.previousData.adminMode.type}</strong></p>
                    )
                } else {
                    display = (
                        <div>
                            <UserModified 
                                product = {product}/>
                        </div>
                    )
                }
                return (
                    <div className = {classes.UserProduct} key = {product.id + product.productId}>
                        <p><strong>{product.productDetails.type.toUpperCase()}</strong></p>
                        <h4>Status: {product.previousData.status}</h4>
                        <p>Product name: {product.productDetails.name}</p>
                        <p>Actual number of products requested for: <strong>{product.previousData.actualQuantity}</strong></p>
                        <p>Number of request you claimed for: <strong>{product.numberOfClaims}</strong></p>
                        {display}
                    </div>
                )
            })
        } else {
            products = <p   
                        style ={{
                            display: 'flex',
                            justifyContent: 'center',
                            width: '100%'
                        }}>You haven't claimed any</p>
        }
        return (
            <div className = {classes.UserHistory}>
                {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products,
        userProducts: state.user.products,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserHistory);