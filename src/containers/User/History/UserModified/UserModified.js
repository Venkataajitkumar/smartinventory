import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '../../../../components/UI/Input/Input';
import Button from '../../../../components/UI/Button/Button';
import * as actions from '../../../../store/actions/index';

class UserModified extends Component {
    state = { 
        inputData: {
            elementType: 'input',
            elementConfig: {
                type: 'number',
                placeholder: 'Number of products to be modified',
                min: "1"
            },
            value: '',
            validation: {
                required: true
            },
            valid: false,
            touched: false,
            valueType: 'number of products'
        }
    }

    inputChangedHandler = (event, maxQuantity) => {
        if(parseInt(event.target.value, 10) <= parseInt(maxQuantity, 10)) {
            this.setState({
                inputData: {
                    ...this.state.inputData,
                    value: event.target.value
                }
            })
        } else {
            this.setState({
                inputData: {
                    ...this.state.inputData,
                    elementConfig: {
                        ...this.state.inputData.elementConfig,
                        placeholder: 'Maximum amount reached you may enter upto ' + maxQuantity
                    },
                    value: ''
                }
            })
        }
    }

    dataSubmitHandler = (data, id) => {
        const dataToBeProcessed = {
            ...data,
            modifiedRequest: this.state.inputData.value
        }
        this.props.onDataChanged(dataToBeProcessed, id);
    }

    render() {
        return (
            <div>
                <Input 
                    elementType = {this.state.inputData.elementType}
                    elementConfig = {this.state.inputData.elementConfig}
                    value = {this.state.inputData.value}
                    changed = {(event) => this.inputChangedHandler(event, this.props.product.productDetails.quantity)}/>
                <Button
                    clicked = {() => this.dataSubmitHandler(this.props.product.previousData, this.props.product.id)}
                    style = {{
                        display: "flex",
                        justifyContent: "flex-end",
                        width: "100%"
                    }}>MODIFY REQUEST</Button>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDataChanged: (data, id) => dispatch(actions.dataChanged(data, id))
    };
};

export default connect(null, mapDispatchToProps)(UserModified);