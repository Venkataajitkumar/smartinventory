import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '../../../../components/UI/Input/Input';
import Button from '../../../../components/UI/Button/Button';
import classes from './UserPaymentProduct.module.css';
import * as actions from '../../../../store/actions/index';

class UserPaymentProduct extends Component {

    state = { 
        paymentMode: {
            value: 'onlineTransfer'
        },
        bankDetails: {
            bankName: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Bank name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'bank name'
            },
            accountNumber: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Account number',
                    min: "1",
                    step: "1"
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 12,
                    maxLength: 12,
                    isNumeric: true
                },
                valid: false,
                touched: false,
                valueType: 'account number'
            },
            routingNumber: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Routing Number',
                    min: "1",
                    step: "1"
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 9,
                    maxLength: 9,
                    isNumeric: true
                },
                valid: false,
                touched: false,
                valueType: 'quantity'
            }
        },
        formIsValid: false,
        requested: false
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.bankDetails,
            [formElementIdentifier] : {
                ...this.state.bankDetails[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.bankDetails[formElementIdentifier].validation),
                touched: true
            }
        }

        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ bankDetails: updatedControls, formIsValid: formIsValid })
    }

    onDropdownChangedHandler = (event) => {
        let updatedPaymentMode = this.state.paymentMode;
        updatedPaymentMode.value = event.target.value;
        if(event.target.value === "check") {
            this.setState({ paymentMode: updatedPaymentMode, formIsValid: true });
        } else {
            this.setState({ paymentMode: updatedPaymentMode, formIsValid: false });
        }        
    }

    onProductPaymentHandler = (event, data, id, userInfo) => {
        event.preventDefault();
        let paymentMode = null;
        console.log(JSON.stringify(userInfo))
        if(this.state.paymentMode.value === "onlineTransfer") {
            paymentMode = {
                type: this.state.paymentMode.value,
                bankDetails: {
                    bankName: this.state.bankDetails.bankName.value,
                    accountNumber: this.state.bankDetails.accountNumber.value,
                    routingNumber: this.state.bankDetails.routingNumber.value
                }
            }
        } else {
            paymentMode = {
                type: this.state.paymentMode.value,
                address: {
                    // name: userInfo.name,
                    // email: userInfo.email,
                    // addressLineOne: userInfo.addressLineOne,
                    // addressLineTwo: userInfo.addressLineTwo,
                    // city: userInfo.city,
                    // state: userInfo.state,
                    // zipCode: userInfo.zipCode
                }
            }
        }
        let updatedData = {
            ...data,
            requested: true,
            paymentMode: paymentMode
        }
        this.props.onDataChanged(updatedData, id);
        this.setState({ requested: true })
    }

    render() {
        let options = [
            {
                displayValue: "Transfer to account",
                value: "onlineTransfer"
            },
            {
                displayValue: "Check",
                value: "check"
            }
        ];

        let formInput = null;

        if(this.state.paymentMode.value === "onlineTransfer") {
            let formElementArray = [];

            for (let key in this.state.bankDetails) {
                formElementArray.push({
                    id: key,
                    config: this.state.bankDetails[key]
                });
            }

            formInput = ( 
                    formElementArray.map( formElement => (
                        <Input 
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))
            );
        } else {
            formInput = <label>Will use your signup data</label>
        }

        let form = null;
        if(this.props.url) {
            form = (
                <div className = {classes.UserPaymentProduct}>
                    <label>Product name: {this.props.product.productDetails.name}</label>
                    <p>Your payment has been received <a href= {this.props.url} target = "_blank" rel="noopener noreferrer" download>Download receipt</a></p>
                </div>
            )
        } else {
            if(this.props.requested) {
                form = (
                    <div className = {classes.UserPaymentProduct}>
                        <p style = {{
                            width: "100%",
                            display: "flex",
                            justifyContent: "center"
                        }}>You have been requested for a payment of <span> </span><strong>{this.props.product.productDetails.name}</strong><span> </span> via <span> </span><strong>{this.props.paymentMode.type}</strong></p>
                    </div>
                )
            } else {
                form = (
                    <form
                        onSubmit = {(event) => this.onProductPaymentHandler(event, this.props.product.previousData, this.props.product.id, this.props.userInfo)}
                        className = {classes.UserPaymentProduct}>
                        <label>Product name: {this.props.product.productDetails.name}</label>
                        <label>Number of products shipped: {this.props.product.numberOfClaims}</label>
                        <label>Total price: USD <strong>{this.props.product.productDetails.price * this.props.product.numberOfClaims}</strong></label>
                        <div className = {classes.InputValues} >
                            <label>Please select a payment mode</label>
                            <Input 
                                inputtype = "select"
                                options = {options}
                                changed = {(event) => this.onDropdownChangedHandler(event)}/>
                            {formInput}
                        </div>
                        {this.state.requested ? <p>Requested</p> : <Button disabled = {!this.state.formIsValid}>REQUEST FOR PAYMENT</Button>}
                    </form>
                )
            }
        }
        return form
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onDataChanged: (data, id) => dispatch(actions.dataChanged(data, id))
    }
}

export default connect(null, mapDispatchToProps)(UserPaymentProduct);