import React, { Component } from 'react';
import { connect } from 'react-redux';

import UserPaymentProduct from './UserPaymentProduct/UserPaymentProduct';
import classes from './UserPayment.module.css';
import * as actions from '../../../store/actions/index';

class UserPayment extends Component {

    state = {
        productsToDisplay: [],
        paymentReceived: false
    }

    componentDidMount () {
        this.props.onInitData();
        this.props.onUserFetch();
        let products = [];
        for(let userData of this.props.userProducts) {
            for (let actualProduct of this.props.products) {
                if(userData.productID === actualProduct.id && userData.userId === this.props.userId && userData.confirmed) {
                    let requested = null;
                    let paymentMode = null;
                    let url = null;
                    if(userData.requested) {
                        requested = true;
                        paymentMode = userData.paymentMode;
                    }
                    if(userData.urlReceipt) {
                        url = userData.urlReceipt;
                    }
                    products.push({
                        productDetails: actualProduct,
                        numberOfClaims: userData.claimedProduct,
                        id: userData.id,
                        productId: userData.productID,
                        previousData: userData,
                        requested: requested,
                        paymentMode: paymentMode,
                        paymentReceived: url ? true : false
                    });
                    break;                    
                }
            }
        }
        this.setState({ productsToDisplay: products })
    }

    onPaymentHandler = (paymentReceived) => {
        this.setState({
            paymentReceived: paymentReceived
        })
    }

    render() {        
        let products = this.state.productsToDisplay.map(product => {
            if(product.paymentReceived && this.state.paymentReceived) {
                return <UserPaymentProduct 
                            key = {product.id + product.productId}
                            product = {product}
                            url = {product.previousData.urlReceipt}
                            requested = {product.requested}
                            paymentMode = {product.paymentMode}/>
            }
            if(!product.paymentReceived && !this.state.paymentReceived) {
                let userInfo = null;
                for (let data of this.props.userData) {
                    if(data.userId === product.previousData.userId) {
                        userInfo = data;
                        break;
                    }
                }
                return <UserPaymentProduct 
                            key = {product.id + product.productId}
                            product = {product}
                            url = {null}
                            userInfo = {userInfo}
                            requested = {product.requested}
                            paymentMode = {product.paymentMode}/>
            } else return null;
        })

        let counter = 0;

        for(let data of products) {
            if(data === null) {
                counter++;
            } else {
                break;
            }
        }
        
        if(counter === products.length) {
            if(this.state.paymentReceived) {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No new payments received</p>
                    </div>
                )
            } 
            if (!this.state.paymentReceived) {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>Claim products to request for a payment</p>
                    </div>
                )
            }
        }

        return (
            <div>
                <div className = {classes.NotificationNav}>
                    <p onClick = {() => this.onPaymentHandler(false)}>REQUEST FOR PAYMENTS</p>
                    <p onClick = {() => this.onPaymentHandler(true)}>PAYMENT RECEIVED</p>
                </div>
               {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products,
        userProducts: state.user.products,
        userData: state.userData.userData,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPayment);