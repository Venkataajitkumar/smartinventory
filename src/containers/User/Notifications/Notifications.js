import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './Notifications.module.css';
import Button from '../../../components/UI/Button/Button';
import * as actions from '../../../store/actions/index';

class Notifications extends Component {

    state = {
        productsToDisplay: [],
        mode: "Accepted"
    }

    componentWillMount () {
        this.props.onInitData();
        this.props.onUserFetch();
        let products = [];
        for(let userData of this.props.userProducts) {
            for (let actualProduct of this.props.products) {
                if(userData.productID === actualProduct.id && userData.userId === this.props.userId) {
                    let status = null;
                    if(userData.adminMode) {
                        status = userData.adminMode.type;
                        let shipped = false;
                        if(userData.shipped) {
                            shipped = true;
                        }
                        products.push({
                            productDetails: actualProduct,
                            numberOfClaims: userData.claimedProduct,
                            productId: userData.id,
                            modifiedNumber: userData.adminMode.productChanged, 
                            status: status,
                            userData: userData,
                            previousData: userData,
                            shipped: shipped
                        });
                    }
                    break;
                }
            }
        }
        this.setState({ productsToDisplay: products })
    }

    onModecHangedHandler = (mode) => {
        this.setState({ mode: mode });
    }

    onDeleteRequest = (id) => {
        let updatedProducts = this.state.productsToDisplay.filter(product => product.id !== id);
        this.setState({ productsToDisplay: updatedProducts });
        this.props.onDataDeleteRequest(id);
    }

    onDataChangeHandler = (data, id) => {
        this.props.onDataChanged(data, id);
        const productIndex = this.state.productsToDisplay.findIndex(product => product.id === id);
        const updatedProducts = this.state.productsToDisplay.slice();
        updatedProducts[productIndex] = {
            ...data
        };
        this.setState({ productsToDisplay: updatedProducts });
    }

    render() {
        let products = null;
        if(this.state.productsToDisplay) {
            products = this.state.productsToDisplay.map(product => {
                if ( this.state.mode === "Accepted" && product.status === "Accepted" ) {
                    let dataToBeProcessed = {
                        ...product.userData,
                        status: "Products being shipping",
                        shipped: true
                    }
                    return (
                        <div className = {classes.UserProduct} key = {product.productId}>
                            <p>Message: {product.previousData.adminMode.message}</p>
                            <p>Your product has been <strong>{product.status.toUpperCase()}</strong></p>
                            <p>Product name: {product.productDetails.name}</p>
                            <p>Actual number of products requested for: <strong>{product.previousData.actualQuantity}</strong></p>
                            <p>Number of request you claimed for: <strong>{product.numberOfClaims}</strong></p>
                            <a href = {product.previousData.shippingLabelUrl} target="_blank" rel="noopener noreferrer" download style ={{textDecoration: 'none  '}}>
                                <Button 
                                    clicked = {() => this.onDataChangeHandler(dataToBeProcessed, product.productId)}
                                    style = {{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                        width: "100%"
                                    }}
                                    disabled = {product.shipped}>DOWNLOAD SHIPMENT LABEL</Button>
                            </a>
                        </div>
                    )
                } else if ( this.state.mode === "Rejected" && product.status === "Rejected") {
                    return (
                        <div className = {classes.UserProduct} key = {product.productId}>
                            <p>Message: {product.previousData.adminMode.message}</p>
                            <p>Your product has been <strong>{product.status.toUpperCase()}</strong></p>
                            <p>Product name: {product.productDetails.name}</p>
                            <p>Actual number of products requested for: <strong>{product.previousData.actualQuantity}</strong></p>
                            <p>Number of request you claimed for: <strong>{product.numberOfClaims}</strong></p>
                            <Button 
                                clicked = {() => this.onDeleteRequest(product.productId)}
                                style = {{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    width: "100%"
                                }}>DELETE REQUEST</Button>
                        </div>
                    )
                } else if ( this.state.mode === "Modified" && product.status === "Modified") {
                    let dataToBeProcessed = null;
                    dataToBeProcessed = {
                        ...product.previousData,
                        status: "Products to be shipped.",
                        adminMode: {
                            ...product.previousData.adminMode,
                            type: "Accepted",
                            message: "Your products has been accepted by admin"
                        },
                        claimedProduct: product.previousData.adminMode.productChanged
                    }
                    return (
                        <div className = {classes.UserProduct} key = {product.productId}>
                            <p>Message: {product.previousData.adminMode.message}</p>
                            <p>Your product has been <strong>{product.status.toUpperCase()}</strong></p>
                            <p>Product name: {product.productDetails.name}</p>
                            <p>Number of request you claimed for: <strong>{product.numberOfClaims}</strong></p>
                            <p>Modified request: <strong>{product.modifiedNumber}</strong></p>
                            <Button
                                clicked = {() => this.onDataChangeHandler(dataToBeProcessed, product.productId)}
                                style = {{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    width: "100%"
                                }}>ACCEPT</Button>
                        </div>
                    )
                } else return null
            })
        }

        let counter = 0;

        for(let data of products) {
            if(data === null) {
                counter++;
            } else {
                break;
            }
        }

        if(counter === products.length) {
            if(this.state.mode === "Accepted") {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No products has been accepted by admin.</p>
                    </div>
                )
            } 
            if (this.state.mode === "Rejected") {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No products has been rejected by admin.</p>
                    </div>
                )
            }
            if (this.state.mode === "Modified") {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No products has been modified by admin.</p>
                    </div>
                )
            }
        }

        return (
            <div className = {classes.UserNotifications}>
                <div className = {classes.UserNotificationsNavigation}>
                    <div className = {classes.UserParagraph}>
                        <p onClick = {() => this.onModecHangedHandler("Accepted")}>Accepted products</p>
                    </div>
                    <p onClick = {() => this.onModecHangedHandler("Rejected")}>Declined / Modified products</p>
                </div>
                <div className = {classes.UserProducts}>
                    {this.state.mode === "Rejected" || this.state.mode === "Modified" 
                        ? <div className = {classes.ModifiedNavBar}>
                            <p onClick = {() => this.onModecHangedHandler("Rejected")}>Rejected products</p>
                            <p onClick = {() => this.onModecHangedHandler("Modified")}>Modified products</p>
                        </div> 
                        : null}
                    {products}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products,
        userProducts: state.user.products,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData()),
        onDataDeleteRequest: (id) => dispatch(actions.dataDeleted(id)),
        onDataChanged: (data, id) => dispatch(actions.dataChanged(data, id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);