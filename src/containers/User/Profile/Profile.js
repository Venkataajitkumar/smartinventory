import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import classes from './Profile.module.css';

class Profile extends Component {

    state = {
        userData: null
    }

    componentWillMount() {
        for(let user of this.props.userInfo) {
            if(user.userId === this.props.userId) {
                this.setState({
                    userData: user
                });
                break;
            }
        }
    }

    render() {
        return (
            <div>
               <div className = {classes.ProfileBackground}>
                    <div className = {classes.ProfileMain}>
                        <img src = "https://cdn4.iconfinder.com/data/icons/eldorado-user/40/user-512.png" alt = "profile"  width = "150px" height = "150px"/>
                        <Button>Upload picture</Button>
                    </div>
                    <div className = {classes.ProfileMainContent}>
                        <h2>{this.state.userData.name}</h2>
                        <p>FreeLancer at Study Mania</p>
                        <p><strong>Location</strong> {this.state.userData.city}, {this.state.userData.state}</p>
                        <p><strong>Bio</strong> developed many websites using MERN stack</p>
                    </div>
               </div>
               <div className = {classes.ProfileBackground2}>
                   <p className = {classes.About}>About</p>
                   <div className = {classes.ProfileContent}>
                       <label>Email</label>
                       <p>{this.state.userData.email}</p>
                   </div>
                   <div className = {classes.ProfileContent}>
                       <label>Business Phone</label>
                       <p>(660) 528-0453</p>
                   </div>
               </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userInfo: state.userData.userData,
        userId: state.auth.userId
    };
};

export default connect(mapStateToProps)(Profile);