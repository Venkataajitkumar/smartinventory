import React, { Component } from 'react';
import { connect } from 'react-redux';

import ProductView from './ProductView/ProductView';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Button from '../../../components/UI/Button/Button';
import classes from './UserHome.module.css';
import * as actions from '../../../store/actions/index';

class UserHome extends Component {

    state = {
        type: "smartPhone",
        style: {
            display: "none",
            zIndex: "0"
        },
        value: '',
        conversations: [
            {
                id: "",
                isAdmin: true,
                message: "Hi, User"
            },
            {
                id: "",
                isAdmin: false,
                message: "Hi admin"
            },
            {
                id: "",
                isAdmin: false,
                message: "how are you"
            },
            {
                id: "",
                isAdmin: false,
                message: "Is the product named iPhone X is required"
            },
            {
                id: "",
                isAdmin: false,
                message: "I got half of you asked for"
            },
            {
                id: "",
                isAdmin: false,
                message: "Can you see my request"
            }
        ]
    }

    componentDidMount() {
        this.props.onInitData();
        this.props.onUserFetch();
        this.props.onUserDataFetched();
        this.props.onUserChatFetched();
        let chats = [];
        if(this.props.chats) {
            for (let convo of this.props.chats) {
                if(convo.userId === this.props.userId || (convo.isAdmin && convo.userId === this.props.userId)) {
                    chats.push({
                        isAdmin: convo.isAdmin,
                        message: convo.message
                    })
                }
            }
        }
        this.setState({
            conversations: chats
        })
    }
 
    onTypeChangeHandler = (inputType) => {
        this.setState({ type: inputType });
    }

    onStyleChanged = () => {
        this.setState({
            style: {
                display: "block",
                zIndex: "2" 
            }
        })
    }

    oninputChangedHandler = (event) => {
        this.setState({
            value: event.target.value
        })
    }

    onStyleChangedReversed = () => {
        this.setState({
            style: {
                display: "none",
                zIndex: "0"
            }
        })
    }

    onChatDelivered = () => {
        let conversations = this.state.conversations;
        let data = {
            userId: this.props.userId,
            isAdmin: false,
            message: this.state.value
        };
        this.setState({
            conversations: conversations.concat(data),
            value: ''
        });
        this.props.onUserChatDelivered(data);
    }

    render() {
        let conversations = this.state.conversations.map(convo => {
            let style = null;
            if(convo.isAdmin) {
                style = classes.Admin;
            } else {
                style = classes.User;
            }
            return (
                <div className = {style} key = {convo.message}>
                    <p>{convo.message}</p>
                </div>
            )
        })
        return (
            <Aux>
                <div className = {classes.Button}>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("smartPhone")}>SmartPhones</Button>
                    <Button 
                        clicked = {() => this.onTypeChangeHandler("laptop")}>Laptops</Button>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("PC")}>Personal Computers</Button>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("television")}>Television</Button>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("gaming")}>Gaming</Button>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("others")}>Others</Button>
                    <Button
                        clicked = {() => this.onTypeChangeHandler("all")}>VIEW ALL PRODUCTS</Button>
                </div>
                <div>
                    <ProductView type = {this.state.type} />
                </div>
                <div className = {classes.Chat} onClick = {this.onStyleChanged}>
                    Have a question? Ask us
                </div>  
                <div className = {classes.ChatEnabled} style = {this.state.style}>
                    <div className = {classes.ChatBox}>
                        <Button clicked = {this.onStyleChangedReversed}>Close Chat</Button>
                        <div className = {classes.ChatContent}>
                            <div className = {classes.Conversation}>
                                {conversations}
                            </div>
                            <div className = {classes.Input}>
                                <input type="text" placeholder="Type your message" value = {this.state.value} onChange = {(event) => this.oninputChangedHandler(event)}/>
                                <Button clicked = {this.onChatDelivered}>SEND</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        chats: state.user.chats
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserDataFetched: () => dispatch(actions.userDataFetched()),
        onUserFetch: () => dispatch(actions.initUserData()),
        onUserChatFetched: () => dispatch(actions.chatFetched()),
        onUserChatDelivered: (data) => dispatch(actions.chatDelivered(data))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(UserHome);