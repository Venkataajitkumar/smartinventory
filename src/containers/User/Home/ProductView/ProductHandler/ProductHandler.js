import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './ProductHandler.module.css';
import Input from '../../../../../components/UI/Input/Input';
import Button from '../../../../../components/UI/Button/Button';
import * as actions from '../../../../../store/actions/index';

class ProductHandler extends Component {

    state = {
        controls: {
            claimedProducts: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Number of products to be claimed',
                    min: "1"
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'number of products'
            }
        },
        formIsValid: false,
        disabled: true
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier, quantity) => {
        if(parseInt(quantity, 10) >= parseInt(event.target.value, 10)) {
            const updatedControls = {
                ...this.state.controls,
                [formElementIdentifier] : {
                    ...this.state.controls[formElementIdentifier],
                    value: event.target.value,
                    valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                    touched: true
                }
            }

            let formIsValid = true;
    
            for (let formIdentifier in updatedControls) {
                formIsValid = updatedControls[formIdentifier].valid && formIsValid;
            }
    
            this.setState({ controls: updatedControls, formIsValid: formIsValid })
        }  else {
            const updatedControls = {
                ...this.state.controls,
                [formElementIdentifier] : {
                    ...this.state.controls[formElementIdentifier],
                    elementConfig: {
                        ...this.state.controls[formElementIdentifier].elementConfig,
                        placeholder: 'Maximum amount reached'
                    },
                    value: '',
                    valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                    touched: true
                }
            }

            let formIsValid = true;
    
            for (let formIdentifier in updatedControls) {
                formIsValid = updatedControls[formIdentifier].valid && formIsValid;
            }
    
            this.setState({ controls: updatedControls, formIsValid: formIsValid })
        }  
        if(event.target.value.length !== 0) {
            this.setState({ disabled: false })
        }
    }

    onClaimProducts = (product) => {
        let dataToProcess = {
            productID: product.id,
            claimedProduct: this.state.controls.claimedProducts.value,
            userId: this.props.userId,
            actualQuantity: product.quantity,
            status: "Products under claimed"
        }
        this.setState({ 
            controls: {
                ...this.state.controls,
                claimedProducts: {
                    ...this.state.controls.claimedProducts,
                    elementConfig: {
                        ...this.state.controls.claimedProducts.elementConfig,
                        placeholder: 'Your claim under process'
                    },
                    value: ''
                }
            },
            disabled: true 
        });
        this.props.onUserClaim(dataToProcess);
        setTimeout(() => {}, 5);
    }

    render() {

        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = ( formElementArray.map( formElement => {
                        let elementConfig = {
                            ...formElement.config.elementConfig,
                            placeholder: "Your claim under process"
                        }
                        let value = this.props.userProduct ? '' : formElement.config.value
                        return (
                            <Input 
                                key = {formElement.id}
                                elementType = {formElement.config.elementType}
                                elementConfig = {this.props.userProduct ? elementConfig : formElement.config.elementConfig}
                                value = {value}
                                valueType = {formElement.config.valueType}
                                shouldValidate = {formElement.config.validation}
                                touched = {formElement.config.touched}
                                invalid = {!formElement.config.valid}
                                changed = {(event) => this.inputChangedHandler(event, formElement.id, this.props.quantityRemained)}/>
                        )
                    })
        );

        return (
            <div>
                <div className = {classes.Product}>
                    {form}
                </div>
                <div className = {classes.Button}>
                    <Button
                        disabled = {this.state.disabled || this.props.disabled}
                        clicked = {() => this.onClaimProducts(this.props.product)}>CLAIM</Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserClaim: (data) => dispatch(actions.userClaimedProduct(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductHandler);