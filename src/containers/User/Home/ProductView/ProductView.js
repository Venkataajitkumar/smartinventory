import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import classes from './ProductView.module.css';
import ProductHandler from './ProductHandler/ProductHandler';
import * as actions from '../../../../store/actions/index';

class ProductView extends Component {
    render() {
        let products = null;
        let counter = 0;
        let disabled = false;
        if (this.props.Products.length === 0) {
            counter = 1;
        }

        if (counter === 0) {
                
            if (this.props.type === "all") {
                products = this.props.Products.map(product => {
                    let userProduct = null;
                    let quantityClaimed = 0;
                    for (let data of this.props.userProducts) {
                        if(data.adminMode) {
                            if(product.id === data.productID && !data.adminMode.type) {
                                quantityClaimed += parseInt(data.claimedProduct, 10);
                            }
                        } else {
                            if(product.id === data.productID && this.props.userId === data.userId) {
                                disabled = true;
                                userProduct = data;
                            }
                            if(product.id === data.productID ) {
                                quantityClaimed += parseInt(data.claimedProduct, 10);
                            }
                        }
                    }
                    return (
                        <div className = {classes.Products} key = {product.id}>
                            <div className = {classes.Product}>
                                <strong style = {{ marginBottom: "20px", textDecoration: "underline" }}>{product.type.toUpperCase()}</strong>
                            </div>
                            <div className = {classes.ProductPic}>
                                <img src = {product.productPicURL} alt = "product view" width = "300px" height = "200px"/>
                            </div>
                            <div className = {classes.Product}>
                                <label>Product Name: </label>
                                <p>{product.name}</p>
                            </div>
                            <div className = {classes.Product}>
                                <label>About product:</label>
                                <p>{product.description}</p>
                            </div>
                            <div className = {classes.Product}>
                                <label>Number of products needed:</label>
                                <p>{parseInt(product.quantity, 10) - quantityClaimed}</p>
                            </div>
                            <div className = {classes.Product}>
                                <label>Price per line:</label>
                                <p>{product.price}</p>
                            </div>
                            <ProductHandler 
                                userProduct = {userProduct}
                                product = {product}
                                disabled = {disabled}/>
                            {disabled = false}
                        </div>                    
                    );
                    
                });
            } else {
                let numberOfProducts = 0;
                products = this.props.Products.map(product => {
                    let userProduct = null;
                    let quantityClaimed = 0;
                    for (let data of this.props.userProducts) {
                        if(data.adminMode) {
                            if(product.id === data.productID && !data.adminMode.type) {
                                quantityClaimed += parseInt(data.claimedProduct, 10);
                            }
                        } else {
                            if(product.id === data.productID && this.props.userId === data.userId) {
                                disabled = true;
                                userProduct = data;
                            }
                            if(product.id === data.productID ) {
                                quantityClaimed += parseInt(data.claimedProduct, 10);
                            }
                        }
                    }
                    if(product.type === this.props.type) {
                        numberOfProducts++;
                        return (
                            <div className = {classes.Products} key = {product.id}>
                                <div className = {classes.Product}>
                                    <strong style = {{ marginBottom: "20px", textDecoration: "underline" }}>{product.type.toUpperCase()}</strong>
                                </div>
                                <div className = {classes.ProductPic}>
                                    <img src = {product.productPicURL} alt = "product view" width = "300px" height = "200px" />
                                </div>
                                <div className = {classes.Product}>
                                    <label>Product Name: </label>
                                    <p>{product.name}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>About product:</label>
                                    <p>{product.description}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>Number of products needed:</label>
                                    <p>{parseInt(product.quantity, 10) - quantityClaimed}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>Price per line:</label>
                                    <p>{product.price}</p>
                                </div>
                                <ProductHandler 
                                    userProduct = {userProduct}
                                    product = {product}
                                    quantityRemained = {parseInt(product.quantity, 10) - quantityClaimed}
                                    disabled = {disabled}/>
                            </div>                    
                        );
                    } else return null
                });
    
                if (numberOfProducts === 0) {
                    products = <p>No products on <strong>{this.props.type.toUpperCase()}</strong>, please try other</p>
                }
            } 
        } else {
            products = <p>No products has been added yet</p>
        }

        return (
            <div className = {classes.ProductsView}>
                {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        Products: state.admin.products,
        userProducts: state.user.products,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserClaim: (data) => dispatch(actions.userClaimedProduct(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductView));