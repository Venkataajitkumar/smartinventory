import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import classes from './Notifications.module.css';
import Input from '../../../components/UI/Input/Input';
import  { storage } from '../../../firebase/index';
import * as actions from '../../../store/actions/index';

class Notifications extends Component {

    state = {
        productsToDisplay: [],
        shipped: false,
        input: {
            elementType: 'input',
            elementConfig: {
                type: "number",
                placeholder: "New number of products to be changed",
                min: "1",
                step: "1"
            },
            value: ''
        },
        selectedFile: null
    }

    componentWillMount () {    
        this.props.onInitData();    
        this.props.onUserFetch();
        this.props.onUserDataFetched();  
        let products = [];
        for(let userData of this.props.userProducts) {
            for (let actualProduct of this.props.products) {
                if(userData.productID === actualProduct.id) {
                    let type = null;
                    let shipped = false;
                    let modifiedrequest = null;
                    if (userData.adminMode) {
                        type = userData.adminMode.type;
                    }
                    if(userData.shipped) {
                        shipped = userData.shipped;
                    }
                    if(userData.modifiedRequest) {
                        modifiedrequest = userData.modifiedRequest;
                    }
                    products.push({
                        productDetails: actualProduct,
                        numberOfClaims: userData.claimedProduct,
                        id: userData.id,
                        type: type,
                        productShipped: shipped,
                        productId: userData.productID,
                        userId: userData.userId,
                        previousData: userData,
                        modifiedrequest: modifiedrequest
                    });
                    break;
                }
            }
        }
        this.setState({ productsToDisplay: products });
    }

    onShipmentHandler = (shipment) => {
        this.setState({ shipped: shipment });
    }

    onUserProductsHandler = (previousData, mode, productQuantityChanged, id, product, message) => {
        let dataToBeInserted = {
            ...previousData,
            status: "Products to be shipped.",
            adminMode: {
                type: mode,
                productChanged: productQuantityChanged,
                message: message
            }
        };
        this.props.onDataChanged(dataToBeInserted, id);
        const productIndex = this.state.productsToDisplay.findIndex(product => product.id === id);
        const updatedProducts = this.state.productsToDisplay.slice();
        updatedProducts[productIndex] = {
            ...product,
            type: mode
        };
        this.setState({ productsToDisplay: updatedProducts });
        setTimeout(() => {}, 5);
    }

    onConfirmGoods = (previousData, id, productDetails, confirmed) => {
        if(confirmed) {
            let data = {
                ...previousData,
                status: "Products shipped successfully",
                confirmed: true
            };
            let quantity = parseInt(productDetails.quantity, 10) - parseInt(previousData.claimedProduct, 10);
            let dataToBeProcessed = {
                ...productDetails,
                quantity: quantity
            }
            this.props.onDataChanged(data, id);
            this.props.onProductEdit(productDetails.id, dataToBeProcessed, null);
        } else {
            let data = {
                ...previousData,
                confirmed: false
            };
            this.props.onDataChanged(data, id);
        }
    }

    onModifiedGoods = (previousData, id, modifiedGoods, mode, message, requestAccepted) => {
        let data = null;
        if(requestAccepted) {
            data = {
                ...previousData,
                claimedProduct: modifiedGoods,
                modifiedRequest: null,
                status: "Products to be shipped.",
                adminMode: {
                    type: mode,
                    message: message
                }
            }
        } else {
            data = {
                ...previousData,
                modifiedRequest: null,
                adminMode: {
                    type: mode,
                    message: message
                }
            }
        }
        this.props.onDataChanged(data, id);
    }

    onInputChangeHandler = (event, previousValue) => {
        if(parseInt(previousValue, 10) >= parseInt(event.target.value, 10)) {
            this.setState({
                input: {
                    ...this.state.input,
                    value: event.target.value
                }
            })
        } else {    
            this.setState({
                input: {
                    ...this.state.input,
                    elementConfig: {
                        ...this.state.input.elementConfig,
                        placeholder: "You have modified more than the required products"
                    }
                }
            })
        }
    }

    fileSelectHandler = (event) => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    fileUploadHandler = (userData, mode, productQuantityChanged, message) => {
        const uploadTask = storage.ref(`shipppingLabel/${userData.previousData.id}`).put(this.state.selectedFile)
        uploadTask.on('state_changed', 
        (snapshot) => {}, (error) => {}, 
        () => {
            storage.ref('shipppingLabel').child(userData.previousData.id).getDownloadURL().then(url => {
                const dataToBeProceesed = {
                    ...userData.previousData,
                    shippingLabelUrl: url,
                    adminMode: {
                        type: mode,
                        productChanged: productQuantityChanged,
                        message: message
                    }
                } 
                this.props.onDataChanged(dataToBeProceesed, userData.previousData.id);
            })
        })
    }

    render() {

        let products = this.state.productsToDisplay.map(product => {
            let user = null
            for(let userFromData of this.props.userData) {
                if(product.userId === userFromData.userId) {
                    user = userFromData;
                }
            }
            if(this.state.shipped === false && !product.modifiedrequest) {
                let previousData = {
                    claimedProduct: product.numberOfClaims,
                    productID: product.productId,
                    userId: product.userId,
                    actualQuantity: product.productDetails.quantity
                };
                if (!product.type || product.type === "Accepted" || product.type === "Modified") {
                    let productButtons = (
                        <div>
                            <Input 
                                elementType = {this.state.input.elementType}
                                elementConfig = {this.state.input.elementConfig}
                                value = {this.state.input.value}
                                changed = {(event) => this.onInputChangeHandler(event, product.productDetails.quantity)}/>
                            <div 
                                style = {{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                }}>
                                <Button 
                                    clicked = {() => this.onUserProductsHandler(previousData, "Accepted", null, product.id, product, "Your products has been accepted by admin")}
                                    style ={{margin: '0px', padding: '0px 10px'}}>ACCEPT</Button>
                                <Button 
                                    clicked = {() => this.onUserProductsHandler(previousData, "Modified", this.state.input.value, product.id, product, "Your products has been modified by admin")}
                                    style ={{margin: '0px', padding: '0px 10px', color: "seagreen"}}>MODIFY</Button>
                                <Button 
                                    clicked = {() => this.onUserProductsHandler(previousData, "Rejected", null, product.id, product, "Your products has been rejected by admin")}
                                    style = {{
                                        color: "crimson",
                                        margin: '0px', 
                                        padding: '0px 10px'
                                    }}>DECLINE</Button>
                            </div>
                        </div>
                    );
                    let inputField = null;
                    if(product.type === "Accepted") {
                        productButtons = <div 
                                            style = {{
                                                display: "flex",
                                                justifyContent: "flex-end",
                                            }}>You accepted these products</div>
                        inputField = (
                            <div>
                                {product.previousData.shippingLabelUrl ? null : <input type="file" onChange = {(event) => this.fileSelectHandler(event)}/>}
                                {product.previousData.shippingLabelUrl ? <p>Shipping label successfully uploaded</p> : <Button style = {{
                                    color: "black",
                                    fontWeight: "350"
                                }}
                                clicked = {() => this.fileUploadHandler(product, "Accepted", this.state.input.value, "Your products has been accepted by admin")}>UPLOAD SHIPPING LABEL</Button>}
                            </div>
                        )
                    } else if(product.type === "Rejected") {
                        productButtons = <div 
                                            style = {{
                                                display: "flex",
                                                justifyContent: "flex-end",
                                            }}>You rejected these products</div>
                    } else if(product.type === "Modified") {
                        productButtons = <div 
                                            style = {{
                                                display: "flex",
                                                justifyContent: "flex-end",
                                            }}>You modified these products</div>
                    }
                    return (
                        <div className = {classes.Notifications} key = {product.id}>
                            <p>Request from: {user ? user.name : null}</p>
                            <p style = {{ textDecoration: "underline" }}><strong>{product.productDetails.type.toUpperCase()}</strong></p>
                            <p>Product name: {product.productDetails.name}</p>
                            <p>Number of products requested for: <strong>{product.previousData.actualQuantity}</strong></p>
                            <p>User claimed for: <strong>{product.numberOfClaims}</strong></p>
                            {inputField}
                            {productButtons}
                        </div>
                    )
                }
            } 
            if (this.state.shipped && product.productShipped) {
                return (
                    <div className = {classes.Notifications} key = {product.id}>
                        <p>Request from: {user ? user.name : null}</p>
                        <p style = {{ textDecoration: "underline" }}><strong>{product.productDetails.type.toUpperCase()}</strong></p>
                        <p>Product name: {product.productDetails.name}</p>
                        <p>Number of products requested for: <strong>{product.productDetails.quantity}</strong></p>
                        <p>User claimed for: <strong>{product.numberOfClaims}</strong></p>
                        <div style ={{
                                    display: "flex",
                                    justifyContent: "flex-end",
                                    width: "100%"
                                }}>
                            <Button 
                                clicked = {() => this.onConfirmGoods(product.previousData, product.id, product.productDetails, true)}
                                disabled = {product.previousData.confirmed}>CONFIRM GOODS</Button>
                            <Button 
                                clicked = {() => this.onConfirmGoods(product.previousData, product.id, product.productDetails, false)}
                                disabled = {product.previousData.confirmed === null ? false : product.previousData.confirmed}>REJECT GOODS</Button>
                        </div>
                    </div>
                )
            } 
            if(this.state.shipped === null && product.modifiedrequest) {
                return (
                    <div className = {classes.Notifications} key = {product.id}>
                        <p>Request from: {user ? user.name : null}</p>
                        <p style = {{ textDecoration: "underline" }}><strong>{product.productDetails.type.toUpperCase()}</strong></p>
                        <p>Product name: {product.productDetails.name}</p>
                        <p>Number of products requested for: <strong>{product.productDetails.quantity}</strong></p>
                        <p>Previously user claimed for: <strong>{product.numberOfClaims}</strong></p>
                        <p>Modified number of claims: <strong>{product.modifiedrequest}</strong></p>
                        <div 
                            style ={{
                                display: "flex",
                                justifyContent: "flex-end",
                                width: "100%"
                            }}>
                            <Button
                                clicked = {() => this.onModifiedGoods(product.previousData, product.id, product.modifiedrequest, "Accepted", "Your request on claim has been accepted by admin", true)} 
                                style ={{ color: 'seagreen' }}>ACCEPT</Button>
                            <Button 
                                clicked = {() => this.onModifiedGoods(product.previousData, product.id, product.modifiedrequest, "Accepted", "Your request has rejected but your products has accepted by admin", false)}
                                style ={{ color: 'crimson' }}>REJECT</Button>
                        </div>
                    </div>
                )
            } else return null
        })

        let counter = 0;

        for(let data of products) {
            if(data === null) {
                counter++;
            } else {
                break;
            }
        }
        
        if(counter === products.length) {
            if(this.state.shipped === null) {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No new modified requests.</p>
                    </div>
                )
            } 
            if (this.state.shipped === false) {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No new requests.</p>
                    </div>
                )
            }
            if (this.state.shipped) {
                products = (
                    <div style = {{
                        display: 'flex',
                        justifyContent: 'center',
                        width: '100%'
                    }}>
                        <p>No user has been shipped your products.</p>
                    </div>
                )
            }
        }

        return (
            <div>
                <div className = {classes.NotificationNav}>
                    <p onClick = {() => this.onShipmentHandler(false)}>NEW REQUESTS</p>
                    <p onClick = {() => this.onShipmentHandler(true)}>PRODUCTS ON TRANSIT</p>
                    <p onClick = {() => this.onShipmentHandler(null)}>MODIFIED REQUESTS</p>
                </div>
                {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products,
        userProducts: state.user.products,
        userData: state.userData.userData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onProductEdit: (productID, productData, productIndex) => dispatch(actions.editingProduct(productID, productData, productIndex)),
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData()),
        onUserDataFetched: () => dispatch(actions.userDataFetched()),
        onDataChanged: (data, id) => dispatch(actions.dataChanged(data, id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);