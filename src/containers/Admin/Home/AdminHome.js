import React, { PureComponent } from 'react';
import  { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Aux from '../../../hoc/Auxiliary/Auxiliary';
import classes from './AdminHome.module.css';
import ProductBackground from '../../Admin/Products/AddingProducts/ProductBackground/AddingProductBackground';
import PieChart from '../../../components/Charts/PieChart';
import * as actions from '../../../store/actions/index';

class AdminHome extends PureComponent {

    componentDidMount() {
        this.props.onInitData();
        this.props.onUserFetch();
        this.props.onUserDataFetched();
    }

    render() {
        return (
            <Aux>              
                <div className = {classes.adminHome}>
                    <div className = {classes.PieChart}>
                        <PieChart />
                    </div>
                    <div className = {classes.Box}>
                        <ProductBackground />
                    </div>
                </div>
            </Aux>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData()),
        onUserDataFetched: () => dispatch(actions.userDataFetched())
    }
}

export default connect(null, mapDispatchToProps)(withRouter(AdminHome));