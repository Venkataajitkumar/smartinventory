import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './Chats.module.css';
import Button from '../../../components/UI/Button/Button';
import * as actions from '../../../store/actions/index';

class Chats extends Component {
    state = { 
        userData: [],
        convo: [],
        userId: null,
        value: ''
    }

    componentWillMount() {
        this.props.onUserChatFetched();
        let userInfo = [];
        for (let user of this.props.userInfo) {
            userInfo.push({
                userId: user.userId,
                name: user.name
            })
        }
        let chats = [];
        if(this.props.chats) {
            for (let convo of this.props.chats) {
                chats.push({
                    isAdmin: convo.isAdmin,
                    message: convo.message,
                    userId: convo.userId
                })
            }
        }
        this.setState({
            userData: userInfo,
            convo: chats
        })
    }

    onUserIdChanged = (id) => {
        this.setState({
            userId: id
        })
    }
    
    onInputChangeHandler = (event) => {
        this.setState({
            value: event.target.value
        })
    }

    onChatDelivered = (userID) => {
        let conversations = this.state.convo;
        let data = {
            userId: userID,
            isAdmin: true,
            message: this.state.value
        };
        this.setState({
            convo: conversations.concat(data),
            value: ''
        });
        this.props.onUserChatDelivered(data);
    }

    render() {

        let convoToDisplay = null;
        let inputField = null;
        if(this.state.userId) {

            convoToDisplay = (
                this.state.convo.map(convo => {
                    if((convo.isAdmin && convo.userId === this.state.userId) || convo.userId === this.state.userId) {
                        let style = null;
                        if(convo.isAdmin) {
                            style = classes.Admin;
                        } else {
                            style = classes.UserChat;
                        }
                        return (
                            <div className = {style} key = {convo.message}>
                                <p>{convo.message}</p>
                            </div>
                        )
                    } else return null
                })
            )

            inputField = (
                <div className = {classes.hr}>
                    <input type = "text" placeholder = "Type your message" value = {this.state.value} onChange = {this.onInputChangeHandler} />
                    <Button clicked = {() => this.onChatDelivered(this.state.userId)}>SEND</Button>
                </div>
            )
        } else {
            convoToDisplay = (
                <div>
                    <p>Select a chat to display</p>
                </div>
            )
        }

        let users = this.state.userData.map(user => {
            return (
                <div className = {classes.User} key = {user.userId} onClick = {() => this.onUserIdChanged(user.userId)}>
                    <h3>{user.name}</h3>
                </div>
            )
        })

        return (
            <div className = {classes.Chats}>
                <div className = {classes.UserNames}>
                    {users}
                </div>
                <div className = {classes.ChatScreen}>
                    <div>
                        <p>Chat header</p>
                        <div className = {classes.Conversations}>
                            <div className = {classes.Screen}>
                                {convoToDisplay}
                            </div>
                            {inputField}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userInfo: state.userData.userData,
        chats: state.user.chats
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUserChatFetched: () => dispatch(actions.chatFetched()),
        onUserChatDelivered: (data) => dispatch(actions.chatDelivered(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chats);