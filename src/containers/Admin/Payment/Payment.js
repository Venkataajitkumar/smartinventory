import React, { Component } from 'react';
import { connect } from 'react-redux';
import { storage } from '../../../firebase/index';

import classes from './Payment.module.css';
import Button from '../../../components/UI/Button/Button';
import * as actions from '../../../store/actions/index';

class Payment extends Component {
    state = { 
        productsToDisplay: [],
        selectedFile: null
    }

    componentDidMount () {    
        this.props.onInitData();
        this.props.onUserFetch();
        this.props.onUserDataFetched();     
        let products = [];
        for(let userData of this.props.userProducts) {
            for (let actualProduct of this.props.products) {
                if(userData.productID === actualProduct.id && userData.requested) {
                    for(let userInfo of this.props.userData) {
                        if(userData.userId === userInfo.userId) {
                            products.push({
                                previousData: userData,
                                userInfo: userInfo,
                                productDetails: actualProduct
                            });
                            break;
                        }
                    }
                    break;
                }
            }
            
        }
        this.setState({ productsToDisplay: products, selectedFile: null })
    }

    fileSelectHandler = (event) => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    fileUploadHandler = (userData) => {
        const uploadTask = storage.ref(`payments/${userData.previousData.id}`).put(this.state.selectedFile)
        uploadTask.on('state_changed', 
        (snapshot) => {}, (error) => {}, 
        () => {
            storage.ref('payments').child(userData.previousData.id).getDownloadURL().then(url => {
                const dataToBeProceesed = {
                    ...userData.previousData,
                    urlReceipt: url
                } 
                this.props.onDataChanged(dataToBeProceesed, userData.previousData.id);
            })
        })
    }

    render() {
        let products = this.state.productsToDisplay.map(product => {

            let choosedMethod = null;
            if(product.previousData.paymentMode.type === "onlineTransfer") {
                choosedMethod = (
                    <div className = {classes.PaymentMode}>
                        <h3>User choosed {product.previousData.paymentMode.type} method</h3>
                        <p>Bank Name: {product.previousData.paymentMode.bankDetails.bankName}</p>
                        <p>Account Number: {product.previousData.paymentMode.bankDetails.accountNumber}</p>
                        <p>Routing Number: {product.previousData.paymentMode.bankDetails.routingNumber}</p>
                    </div>
                )
            } else {
                choosedMethod = (
                    <div className = {classes.PaymentMode}>
                        <h3>User choosed {product.previousData.paymentMode.type} method</h3>
                        <p>eMail: {product.userInfo.email}</p>
                        <p>Address Line 1: {product.userInfo.addressLineOne}</p>
                        <p>Address Line 2: {product.userInfo.addressLineTwo}</p>
                        <p>City: {product.userInfo.city}</p>
                        <p>State: {product.userInfo.state}</p>
                        <p>ZIP code: {product.userInfo.zipCode}</p>
                    </div>
                )
            }

            return (
                <div className = {classes.Payment} key = {product.previousData.id + product.previousData.productID}>
                    <p>Name : {product.userInfo.name}</p>
                    <div className = {classes.PaymentDesc}>
                        <p>Product name : {product.productDetails.name}</p>
                        <p>Number of goods received: {product.previousData.claimedProduct}</p>
                        <p>Amount to be payed: USD {parseInt(product.previousData.claimedProduct, 10) * parseInt(product.productDetails.price ,10)}</p>
                    </div>
                    {choosedMethod}
                    {product.previousData.urlReceipt ? null : <input type = "file" onChange = {(event) => this.fileSelectHandler(event)}/>}
                    {product.previousData.urlReceipt ? <p>Payment has been recorded</p> : <Button clicked = {() => this.fileUploadHandler(product)}>PAY</Button>}
                </div>
            )
        })

        let counter = 0;

        for(let data of products) {
            if(data === null) {
                counter++;
            } else {
                break;
            }
        }

        if(counter === products.length) {
            products = (
                <div style = {{
                    display: 'flex',
                    justifyContent: 'center',
                    width: '100%'
                }}>
                    <p>No new payments.</p>
                </div>
            )
        }

        return (
            <div>
                {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products,
        userProducts: state.user.products,
        userData: state.userData.userData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData()),
        onUserDataFetched: () => dispatch(actions.userDataFetched()),
        onDataChanged: (data, id) => dispatch(actions.dataChanged(data, id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);