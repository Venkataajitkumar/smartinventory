import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import classes from './ProductView.module.css';
import Button from '../../../components/UI/Button/Button';
import PrdouctManipulations from './ProductManipulations/ProductManipulations';

class ProductView extends Component {

    state = {
        showDelete: false,
        showEdit: false,
        id: null,
        type: null
    }

    onShowEdit = (productID, type) => {
        let updatedState = this.state.showEdit
        this.setState({ showEdit: !updatedState, id: productID, type: type });
        if(this.state.showEdit) {
            this.props.history.push('/admin/home');
        }
    }

    onShowDelete = (productID, type) => {
        let updatedState = this.state.showDelete
        this.setState({ showDelete: !updatedState, id: productID, type: type });
    }

    render() {

        let products = null;
        let productHeader = null;
        let productOption = null;
        let counter = 0;
        let numberOfProducts = 0;

        for(let product of this.props.Products) {
            if (product.type === this.props.type) {
                numberOfProducts += parseInt(product.quantity, 10)
            }
            if(this.props.type === "all") {
                numberOfProducts += parseInt(product.quantity, 10)
            }
        }

        if(this.props.Products) {
            if (this.props.type !== "all") {
                productHeader = (
                    <div>
                        <p>Type of products viewing: <strong>{this.props.type.toUpperCase()}</strong></p>
                        <p>Number of products on selected type: <strong>{numberOfProducts}</strong></p>
                    </div>
                )
            }
            if (this.props.type !== "all") {
                products = this.props.Products.map(product => {
                    if(product.type === this.props.type) {
                        counter++;
                        return (
                            <div className = {classes.Products} key = {product.id}>
                                <div className = {classes.ProductPic}>
                                    <img src = {product.productPicURL} alt = "product view"  width = "300px" height = "200px"/>
                                </div>
                                <div className = {classes.Product}>
                                    <label>Product Name: </label>
                                    <p>{product.name}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>About product:</label>
                                    <p>{product.description}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>Number of products needed:</label>
                                    <p>{product.quantity}</p>
                                </div>
                                <div className = {classes.Product}>
                                    <label>Price per line:</label>
                                    <p>{product.price}</p>
                                </div>
                                <div className = {classes.ProductButtons}>
                                    <Button 
                                        clicked = {() => this.onShowEdit(product.id, product.type)}
                                        style={{
                                            color: "gold"
                                    }}>EDIT</Button>
                                    <Button
                                        clicked = {() => this.onShowDelete(product.id, product.type)}
                                        style={{
                                            color: "crimson"
                                        }}>DELETE</Button>
                                </div>
                            </div>                    
                        );
                    }      
                    return null       
                })
            }
        }

        if (counter === 0 && this.props.type !== "all") {
            productHeader = (
                <div>
                    <p>Type of products viewing: <strong>{this.props.type.toUpperCase()}</strong></p>
                    <p>No products on selected type</p>
                </div>
            )
        } else if (this.props.type === "all"){
            productHeader = (
                <div>
                    <p>Type of products viewing: <strong>ALL</strong></p>
                    <p>Number of products on selected type: <strong>{numberOfProducts}</strong></p>
                </div>
            )            
            products = this.props.Products.map(product => {
                return (
                    <div className = {classes.Products} key = {product.id}>
                        <div className = {classes.Product}>
                            <strong style = {{ marginBottom: "20px", textDecoration: "underline" }}>{product.type.toUpperCase()}</strong>
                        </div>
                        <div className = {classes.ProductPic}>
                            <img src = {product.productPicURL} alt = "product view"  width = "300px" height = "200px"/>
                        </div>
                        <div className = {classes.Product}>
                            <label>Product Name: </label>
                            <p>{product.name}</p>
                        </div>
                        <div className = {classes.Product}>
                            <label>About product:</label>
                            <p>{product.description}</p>
                        </div>
                        <div className = {classes.Product}>
                            <label>Number of products needed:</label>
                            <p>{product.quantity}</p>
                        </div>
                        <div className = {classes.Product}>
                            <label>Price per line:</label>
                            <p>{product.price}</p>
                        </div>
                        <div className = {classes.ProductButtons}>
                            <Button 
                                clicked = {() => this.onShowEdit(product.id, product.type)}
                                style={{
                                    color: "gold"
                            }}>EDIT</Button>
                            <Button
                                clicked = {() => this.onShowDelete(product.id, product.type)}
                                style={{
                                    color: "crimson"
                                }}>DELETE</Button>
                        </div>
                    </div>                    
                );
            })
        }

        if (this.state.showDelete) {
            productOption = "delete"
        }

        if (this.state.showEdit) {
            productOption = "edit"
        }

        return (
            <div className = {classes.ProductsView}>
                <PrdouctManipulations 
                    type = {productOption} 
                    productType = {this.state.type}
                    productID = {this.state.id}
                    deleteHandler = {this.onShowDelete}
                    editHandler = {this.onShowEdit}
                    showDelete = {this.state.showDelete}
                    showEdit = {this.state.showEdit}/>
                {productHeader}
                {products}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        Products: state.admin.products,
        type: state.admin.type
    }
}

export default connect(mapStateToProps)(withRouter(ProductView));