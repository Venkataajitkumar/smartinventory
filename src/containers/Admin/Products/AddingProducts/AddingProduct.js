import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { storage } from '../../../../firebase/index';

import Input from '../../../../components/UI/Input/Input';
import Button from '../../../../components/UI/Button/Button';
import * as actions from '../../../../store/actions/index';

class AddingProduct extends Component {

    state = {
        controls: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Product name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'name'
            },
            description: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Description of the product'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'description'
            },
            quantity: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Quantity',
                    min: "1",
                    step: "1"
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'quantity'
            },
            price: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Price',
                    min: "1"
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'price'
            }
        },
        selectedFile: null,
        formIsValid: false,
        type: "smartPhone",
        index: 0
    }

    fileSelectHandler = (event) => {
        this.setState({
            selectedFile: event.target.files[0]
        })
    }

    fileUploadHandler = () => {
        const uploadTask = storage.ref(`products/${this.state.controls.name.value}`).put(this.state.selectedFile)
        uploadTask.on('state_changed', 
        (snapshot) => {}, (error) => {}, 
        () => {
            storage.ref('products').child(this.state.controls.name.value).getDownloadURL().then(url => {
                let productDetails = {
                    type: this.state.type,
                    name: this.state.controls.name.value,
                    description: this.state.controls.description.value,
                    quantity: this.state.controls.quantity.value,
                    price: this.state.controls.price.value,
                    productPicURL: url
                }; 
                this.props.onProductAdding(productDetails);
            })
        })
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }
        
        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        let index = this.state.index;
        switch (this.state.type) {
            case "laptop" :
                index = 1;
                break;
            case "PC" :
                index = 2;
                break;
            case "television" :
                index = 3;
                break;
            case "gaming" :
                index = 4;
                break;
            case "others" :
                index = 5;
                break;
            default :
                index = 0;
                break;
        };

        this.setState({ controls: updatedControls, formIsValid: formIsValid, index: index })
    }

    onClickEventHandler = () => {
        this.props.onTypeEdit(this.state.type)
        this.props.history.push(this.props.match.path + "/products");
    }

    dropdownChangedHandler = (event, inputIdentifier) => {
        let index = this.state.index;
        switch (this.state.type) {
            case "laptop" :
                index = 1;
                break;
            case "PC" :
                index = 2;
                break;
            case "television" :
                index = 3;
                break;
            case "gaming" :
                index = 4;
                break;
            case "others" :
                index = 5;
                break;
            default :
                index = 0;
                break;
        };
        this.setState({ [inputIdentifier]: event.target.value, index: index });
    }

    render() {
        let options = [
            {
                displayValue: "SmartPhones",
                value: "smartPhone"
            },
            {
                displayValue: "Laptops",
                value: "laptop"
            },
            {
                displayValue: "Personal Computers",
                value: "PC"
            },
            {
                displayValue: "Television",
                value: "television"
            },
            {
                displayValue: "Gaming",
                value: "gaming"
            },
            {
                displayValue: "Others",
                value: "others"
            },
        ]


        let formElementArray = [];

        for (let key in this.state.controls) {
            formElementArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = ( 
            <form onSubmit = {(e) => {
                this.props.clicked(e);
                this.fileUploadHandler();
                this.props.onDataAdded(this.state.index);                        
                this.onClickEventHandler();
            }}>
                {
                    formElementArray.map( formElement => (
                        <Input 
                            key = {formElement.id}
                            elementType = {formElement.config.elementType}
                            elementConfig = {formElement.config.elementConfig}
                            value = {formElement.config.value}
                            valueType = {formElement.config.valueType}
                            shouldValidate = {formElement.config.validation}
                            touched = {formElement.config.touched}
                            invalid = {!formElement.config.valid}
                            changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                    ))
                }
                <input type="file" onChange = {(event) => this.fileSelectHandler(event)}/>
                <Button 
                    style = {{
                        width: "100%",
                        margin: "auto"
                    }}
                    disabled = {!this.state.formIsValid}>Add your product</Button>
            </form>
        );
    
        return (
            <div>
                <Input 
                    inputtype = "select"
                    options = {options}
                    changed = {(event) => this.dropdownChangedHandler(event, "type")}/>
                {form}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onProductAdding: (productDetails) => dispatch(actions.saveProduct(productDetails)),
        onTypeEdit: (type) => dispatch(actions.typeChange(type)),
        onDataAdded: (index) => dispatch(actions.dataAdded(index))
    }
}

export default connect(null, mapDispatchToProps)(withRouter(AddingProduct));