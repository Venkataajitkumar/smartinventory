import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';


import Button from '../../../../../components/UI/Button/Button';
import Modal from '../../../../../components/UI/Modal/Modal';
import AddProduct from '../AddingProduct';
import classes from './AddingProductBackground.module.css';
import * as actionTypes from '../../../../../store/actions/actionTypes';



class AddingProductBackground extends Component {

    state = { 
        productAdded: false,
        numberProducts: this.props.totalNumberOfProducts
    }

    onProductAdding = () => {
        this.setState({ productAdded: true });
    }

    onProductAdded = () => {
        this.setState({ productAdded: false });
    }

    onViewProduct = (type) => {
        this.props.onTypeEdit(type)
        this.props.history.push(this.props.match.path + "/products");
    }

    render() {
        return (
            <div>
                <Modal show = {this.state.productAdded}>
                    <AddProduct clicked = {this.onProductAdded}/>
                </Modal>
                <Button 
                    clicked = {this.onProductAdding}
                    style = {{
                        position: "absolute",
                        right: "0px",
                    }} >ADD PRODUCT</Button>
                <p>Total number of products requested for: <strong>{this.props.totalNumberOfProducts}</strong></p>
                <div className = {classes.AddingProductBackground}>
                    <p>SmartPhones: <Button 
                                        clicked = {() => this.onViewProduct("smartPhone")} 
                                        style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                    <p>Laptops: <Button 
                                    clicked = {() => this.onViewProduct("laptop")} 
                                    style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                    <p>Personal Computers: <Button 
                                                clicked = {() => this.onViewProduct("PC")} 
                                                style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                    <p>Television: <Button 
                                        clicked = {() => this.onViewProduct("television")} 
                                        style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                    <p>Gaming: <Button 
                                    clicked = {() => this.onViewProduct("gaming")} 
                                    style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                    <p>Others: <Button 
                                    clicked = {() => this.onViewProduct("others")} 
                                    style = {{margin: "0px"}}>VIEW PRODUCTS</Button></p>
                </div>
                <div className = {classes.ViewAll}>
                    <Button 
                        clicked = {() => this.onViewProduct("all")}>
                        VIEW ALL PRODUCTS
                    </Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products : state.admin.products,
        totalNumberOfProducts: state.admin.totalNumberOfProducts
    }
}

const mapDisptachToProps = dispatch => {
    return {
        onTypeEdit: (type) => dispatch({type: actionTypes.TYPE_CHANGE, payload: type})
    }
}

export default connect(mapStateToProps, mapDisptachToProps)(withRouter(AddingProductBackground));