import React, { Component } from 'react';
import { connect } from 'react-redux';

import Modal from '../../../../components/UI/Modal/Modal';
import Aux from '../../../../hoc/Auxiliary/Auxiliary';
import Button from '../../../../components/UI/Button/Button';
import Input from '../../../../components/UI/Input/Input';
import * as actions from '../../../../store/actions/index';

class ProductManipulations extends Component {

    state = {
        controls: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'name'
            },
            description: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Description of the product'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'description'
            },
            quantity: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Quantity',
                    min: "1",
                    step: "1"
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'quantity'
            },
            price: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Price',
                    min: "1"
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                valueType: 'price'
            }
        },
        formIsValid: false
    }

    checkValidity (value, rules) {
        let isValid = true;

        if(!rules) {
            return true;
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, formElementIdentifier) => {
        const updatedControls = {
            ...this.state.controls,
            [formElementIdentifier] : {
                ...this.state.controls[formElementIdentifier],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[formElementIdentifier].validation),
                touched: true
            }
        }
        
        let formIsValid = true;

        for (let formIdentifier in updatedControls) {
            formIsValid = updatedControls[formIdentifier].valid && formIsValid;
        }

        this.setState({ controls: updatedControls, formIsValid: formIsValid })
    }

    onProductEditHandler = (productIndex, productURL) => {
        let product = {
            type: this.props.productType,
            name: this.state.controls.name.value,
            description: this.state.controls.description.value,
            quantity: this.state.controls.quantity.value,
            price: this.state.controls.price.value,
            productPicURL: productURL
        }
        this.props.onProductEdit(this.props.productID, product, productIndex );
    }

    render() {
        let productOption = null;
        let product = null;
        let index = null;
        let productIndex = null;
        let quantity = null;
        let counter = 0;

        

        for (let productData of this.props.products) {
            if(productData.id === this.props.productID) {
                product = productData;
                productIndex = counter;
                quantity = parseInt(productData.quantity, 10)
                switch(product.type) {
                    case "laptop" :
                        index = 1;
                        break;
                    case "PC" :
                        index = 2;
                        break;
                    case "television" :
                        index = 3;
                        break;
                    case "gaming" :
                        index = 4;
                        break;
                    case "others" :
                        index = 5;
                        break;
                    default :
                        index = 0;
                        break;
                }
                break;
            }
            counter++;
        }


        if (this.props.type  === "edit") {

            let formElementArray = [];

            for (let key in this.state.controls) {
                formElementArray.push({
                    id: key,
                    config: this.state.controls[key]
                });
            }

            let form = ( 
                <form onSubmit = {(e) => {
                    this.props.editHandler(e);
                    this.onProductEditHandler(productIndex, product.productPicURL);
                }}>{
                        formElementArray.map( formElement => {
                            let elementConfig = {
                                ...formElement.config.elementConfig,
                                placeholder: product[formElement.id],
                            }
                            return(
                                <Input 
                                key = {formElement.id}
                                elementType = {formElement.config.elementType}
                                elementConfig = {elementConfig}
                                value = {formElement.config.value}
                                valueType = {formElement.config.valueType}
                                shouldValidate = {formElement.config.validation}
                                touched = {formElement.config.touched}
                                invalid = {!formElement.config.valid}
                                changed = {(event) => this.inputChangedHandler(event, formElement.id)}/>
                            )
                        })
                    }
                    <Button 
                        style = {{
                            width: "100%",
                            margin: "auto" 
                        }} 
                        disabled = {!this.state.formIsValid}>EDIT</Button>
                </form>
            );

            productOption = (
                <div>
                    {form}               
                </div>
            );
        } 
        
        if (this.props.type === "delete") {
            productOption = (
                <div>
                    <p>Your about to delete following product</p>
                    <div><label>NAME: <strong>{product.name}</strong></label></div>
                    <div><label>DESCRIPTION: {product.description}</label></div>
                    <div><label>QUANTITY: {product.quantity}</label></div>
                    <div><label>PRICE: {product.price}</label></div>
                    <Button clicked = {(e) => {
                        this.props.deleteHandler(e);
                        this.props.onProductDelete(this.props.productID, quantity, index);
                    }}>DELETE</Button>
                    
                </div>
            );
        }

        return (
            <Aux>
                <Modal show = {this.props.showDelete}>
                    {productOption}
                </Modal>
                <Modal show = {this.props.showEdit}>
                    {productOption}
                </Modal>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.admin.products
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onProductEdit: (productID, productData, productIndex) => dispatch(actions.editingProduct(productID, productData, productIndex)),
        onProductDelete: (productID, quantity, productIndex) => dispatch(actions.delProduct(productID, quantity, productIndex))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductManipulations);