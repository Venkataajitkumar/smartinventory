import * as actionTypes from '../actions/actionTypes';

const initialState = {
    products: [],
    totalNumberOfProducts: 0,
    type: "smartPhone",
    data: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_DATA_SUCCESS: 
            return {
                ...state,
                products: action.products,
                totalNumberOfProducts: action.totalProducts,
                data: action.data
            }
        case actionTypes.ADD_PRODUCT: 
            console.log(state.products)
            return {
                ...state,
                products: state.products.concat({
                    id: action.payload.details.id,
                    type: action.payload.details.type,
                    name: action.payload.details.name,
                    description: action.payload.details.description,
                    quantity: action.payload.details.quantity,
                    price: action.payload.details.price
                }),
                totalNumberOfProducts: state.totalNumberOfProducts + parseInt(action.payload.details.quantity, 10)
            }
        case actionTypes.TYPE_CHANGE:
            return {
                ...state,
                type: action.payload
            }
        case actionTypes.DATA_CHANGE:
            let updatedData = state.data;
            updatedData[action.payload] = state.data[action.payload] + 1;
            return {
                ...state,
                data: updatedData
            }
        case actionTypes.EDIT_PRODUCT:
            let products = state.products.slice();
            // let updatedProduct = {...products[action.payload.index]};
            let productindex = products.findIndex(product => product.id === action.payload.id);
            let updatedProduct = {...products[productindex]};
            let totalProductQuantity = state.totalNumberOfProducts;
            totalProductQuantity = totalProductQuantity - parseInt(updatedProduct.quantity) + parseInt(action.payload.productData.quantity, 10);
            updatedProduct.name = action.payload.productData.name;
            updatedProduct.description = action.payload.productData.description;
            updatedProduct.quantity = action.payload.productData.quantity;
            updatedProduct.price = action.payload.productData.price;
            products[action.payload.index] = updatedProduct;
            return {
                ...state,
                products: products,
                totalNumberOfProducts: totalProductQuantity
            }
        case actionTypes.DELETE_PRODUCT:
            let updatedProducts = state.products.filter(result => result.id !== action.payload.id);
            let updatedProductData = state.data;
            updatedProductData[action.payload.productIndex] = state.data[action.payload.productIndex] - 1;
            return {
                ...state,
                products: updatedProducts,
                data: updatedProductData,
                totalNumberOfProducts: state.totalNumberOfProducts - parseInt(action.payload.quantity, 10)
            }
        default:
            return state
    }
};

export default reducer;