import * as actionTypes from '../actions/actionTypes';

const initialState = {
    userId: null,
    token: null,
    error: null,
    admin: false
}

const reducer = (state =  initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_SUCCESS: 
            return {
                ...state,
                userId: action.userId,
                token: action.tokenId,
                admin: action.admin
            }
        case actionTypes.AUTH_LOGOUT:
            return {
                ...state,
                userId: null,
                token: null,
                admin: false
            }
        case actionTypes.AUTH_FAIL:
            return {
                ...state,
                error: action.error
            }
        default:
          return state;
    }
}

export default reducer;