import * as actionTypes from '../actions/actionTypes';

const initalState = {
    userData: []
}

const reducer = (state = initalState, action) => {
    switch (action.type) {
        case actionTypes.USER_DATA_FETCHED:
            return {
                ...state,
                userData: action.userData
            }
        case actionTypes.USER_DATA_SUCCESS:
            return {
                ...state,
                userData: state.userData.concat(action.userData)
            }
        default:
            return state
    }
}

export default reducer;