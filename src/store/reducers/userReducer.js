import * as actionTypes from '../actions/actionTypes';

const initialState = {
    products: [],
    chats: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USER_DETAILPRODUCT_SUCCESS: 
            return {
                ...state,
                products: action.products
            }
        case actionTypes.USER_CLAIMED:
            return {
                ...state,
                products: state.products.concat({
                    productID: action.data.productID,
                    claimedProduct: action.data.claimedProduct
                })
            }
        case actionTypes.USER_DATA_CHANGED:
            let updatedProducts =state.products.slice();
            const productIndex = state.products.findIndex(product => product.id === action.id)
            let updatedProduct = updatedProducts[productIndex];
            updatedProduct = {
                ...action.data,
                id: action.id
            };
            updatedProducts[productIndex] = updatedProduct;
            return {
                products: updatedProducts
            }
        case actionTypes.USER_DATA_DELETED:
            let updatedProds = state.products.filter(product => product.id !== action.id);
            return {
                ...state,
                products: updatedProds
            }
        case actionTypes.CHAT_DELIVERED:
            return {
                ...state,
                chats: state.chats.concat(action.data)
            }
        case actionTypes.CHAT_FETCHED:
            return {
                ...state,
                chats: action.data
            }
        default:
            return state
    }
}

export default reducer;