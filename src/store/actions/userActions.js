import * as actionTypes from './actionTypes';
import axios from '../../axios-smart-inventory';

export const fetchUserDetailproductSuccess = (products) => {
    return {
        type: actionTypes.FETCH_USER_DETAILPRODUCT_SUCCESS,
        products: products
    };
};

export const initUserData = () => {
    return dispatch => {
        axios.get('/user.json')
            .then(res => {
                let fetchedOrders = [];
                for ( let key in res.data) {
                    fetchedOrders.push({
                        ...res.data[key],
                        id: key
                    });
                }
                dispatch(fetchUserDetailproductSuccess(fetchedOrders));
            })
    };
};

export const userClaimed = (data) => {
    return {
        type: actionTypes.USER_CLAIMED,
        data: data
    };
};

export const userClaimedProduct = (data) => {
    return dispatch => {
        axios.post('/user.json', data)
            .then(res => {
                dispatch(userClaimed(data))
            })
    };
};

export const dataToBeChanged = (data, productID) => {
    return {
        type: actionTypes.USER_DATA_CHANGED,
        data: data,
        id: productID
    };
};

export const dataChanged = (data, productID) => {
    return dispatch => {
        axios.put('/user/' + productID + '.json', data) 
            .then(res => {
                dispatch(dataToBeChanged(data, productID));
            })
    };
};

export const dataToBeDeleted = (productID) => {
    return {
        type: actionTypes.USER_DATA_DELETED,
        id: productID
    };
};

export const dataDeleted = (productID) => {
    return dispatch => {
        axios.delete('/user/' + productID + '.json')
            .then(res => {
                dispatch(dataToBeDeleted(productID))
            })
    };
};

export const onChatDelivered = (data) => {
    return {
        type: actionTypes.CHAT_DELIVERED,
        data: data
    }
}

export const chatDelivered = (data) => {
    return dispatch => {
        axios.post('/chats.json', data)
            .then(response => {
                dispatch(onChatDelivered(data));
            })
    }
}

export const onChatFetched = (data) => {
    return {
        type: actionTypes.CHAT_FETCHED,
        data: data
    }
}

export const chatFetched = () => {
    return dispatch => {
        axios.get('/chats.json')
            .then(response => {
                let fetchedChats = [];
                for ( let key in response.data) {
                    fetchedChats.push({
                        ...response.data[key],
                        id: key
                    });
                }
                dispatch(onChatFetched(fetchedChats))
            })
    }
}