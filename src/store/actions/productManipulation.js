import * as actionTypes from './actionTypes';
import axios from '../../axios-smart-inventory';

export const fetchDataSuccess = (products, totalProductNumbers, data) => {
    return {
        type: actionTypes.FETCH_DATA_SUCCESS,
        products: products,
        totalProducts: totalProductNumbers,
        data: data
    }
}

export const fetchDataFail = (error) => {
    return {
        type: actionTypes.FETCH_DATA_FAIL
    }
}

const dataNeedToSet = (type) => {
    switch(type) {
        case "laptop" :
            return 1
        case "PC" :
            return 2;
        case "television" :
            return 3;
        case "gaming" :
            return 4;
        case "others" :
            return 5;
        default :
            return 0;
    }
}

export const initData = () => {
    return dispatch => {
        axios.get('/products.json')
            .then(res => {
                let fetchedProducts = [];
                let totalNumberOfProducts = 0;
                let data = [0, 0, 0, 0, 0, 0]
                for ( let key in res.data) {
                    totalNumberOfProducts += parseInt(res.data[key].quantity, 10)
                    fetchedProducts.push({
                        ...res.data[key],
                        id: key
                    });
                    data[dataNeedToSet(res.data[key].type)] += 1; 
                }
                dispatch(fetchDataSuccess(fetchedProducts, totalNumberOfProducts, data))
            })
            .catch(err => {
                dispatch(fetchDataFail(err))
            })
    };
};

export const addProduct = (productDetails) => {
    return {
        type: actionTypes.ADD_PRODUCT,
        payload: {
            details: productDetails
        }
    };
};

export const saveProduct = (productDetails) => {
    return dispatch => {
        axios.post('/products.json', productDetails)
            .then(res => {
                dispatch(addProduct(productDetails))
            })
    }
}

export const deleteProduct = (quantity, id, productIndex) => {
    return {
        type: actionTypes.DELETE_PRODUCT,
        payload: {
            id: id,
            quantity: quantity,
            productIndex: productIndex
        }
    }
}

export const delProduct = (productID, quantity, productIndex) => {
    return dispatch => {
        axios.delete('/products/' + productID + ".json")
            .then(res => {
                dispatch(deleteProduct(quantity, productID, productIndex))
            })
    }
}

export const editProduct = (productID, productData, productIndex) => {
    return {
        type: actionTypes.EDIT_PRODUCT,
        payload: {
            id: productID,
            productData: productData,
            index: productIndex
        }
    }
}

export const editingProduct = (productID, productData, productIndex) => {
    return dispatch => {
        axios.put('/products/' + productID + '.json', productData) 
            .then(res => {
                dispatch(editProduct(productID, productData, productIndex))
            })
    }
}

export const typeChange = (typeChange) => {
    return {
        type: actionTypes.TYPE_CHANGE,
        payload: typeChange
    }
}

export const dataAdded = (index) => {
    return {
        type: actionTypes.DATA_CHANGE,
        payload: index
    }
}