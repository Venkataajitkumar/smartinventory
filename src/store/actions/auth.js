import * as actionTypes from './actionTypes';
import * as actions from './index';
import axios from 'axios';

export const authSuccess = (token, userId, isAdmin) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        tokenId: token,
        userId: userId,
        admin: isAdmin
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const authLogout = () => {
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('isAdmin');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkTimeoutValidity = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(authLogout())
        }, expirationTime * 1000);
    }
}

export const auth = (email, password, isSignup, isAdmin, userData) => {
    return dispatch => {
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        }
        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyDkCWJYv91UK74I9x4t-d4WoF6TVXN58dk'
        if( !isSignup ) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDkCWJYv91UK74I9x4t-d4WoF6TVXN58dk'
        }
        axios.post(url, authData)
            .then(response => {
                if(isSignup) {
                    dispatch(actions.userData(userData, response.data.localId))
                }
                let expirationDate = new Date( new Date().getTime() + response.data.expiresIn * 1000 )
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('userId', response.data.localId);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('isAdmin', isAdmin)
                dispatch(authSuccess(response.data.idToken, response.data.localId, isAdmin));
                dispatch(checkTimeoutValidity(response.data.expiresIn));
            })
            .catch(error => {
                dispatch(authFail(error.response.data.error));
            })
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token) {
            dispatch(authLogout());
        } else {
            let expirationDate = new Date(localStorage.getItem('expirationDate'));
            if(expirationDate > new Date()) {
                let userId = localStorage.getItem('userId');
                let isAdmin = localStorage.getItem('isAdmin') !== "false";
                dispatch(authSuccess(token, userId, isAdmin));
                dispatch(checkTimeoutValidity((expirationDate.getTime() - new Date().getTime() ) / 1000 ));
            } else {
                dispatch(authLogout());
            }
        }
    }
}