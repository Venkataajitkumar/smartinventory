import * as actionTypes from './actionTypes';
import axios from '../../axios-smart-inventory';

export const userDataSuccess = (userData) => {
    return {
        type: actionTypes.USER_DATA_SUCCESS,
        userData: userData
    }
}

export const userData = (userData, userId) => {
    return dispatch => {
        let userDataToSend = {
            ...userData,
            userId: userId
        }
        axios.post('/userData.json', userDataToSend)
            .then(response => {
                dispatch(userDataSuccess(userDataToSend));
            })
    }
}

export const userFetched = (userData) => {
    return {
        type: actionTypes.USER_DATA_FETCHED,
        userData: userData
    }
}

export const userDataFetched = () => {
    return dispatch => {
        axios.get('/userData.json')
            .then(response => {
                let fetchedUsers = [];
                for ( let key in response.data) {
                    fetchedUsers.push({
                        ...response.data[key],
                        id: key
                    });
                }
                dispatch(userFetched(fetchedUsers));
            })
    }
}