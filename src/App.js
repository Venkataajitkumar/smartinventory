import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/HomeLayout/Layout'; 
import AdminLayout from './hoc/Layout/AdminLayout/AdminLayout';
import UserLayout from './hoc/Layout/UserLayout/UserLayout';
import * as actions from "./store/actions/index";

class App extends Component {

  componentDidMount() {
    this.props.onTryAutoSignup();
  }

  render() {

    let route = (
      <Switch>
        <Route path = "/" component = {Layout} />
        <Redirect to = "/" />
      </Switch>
    );

    if(this.props.isAuthenticated) {
     if (this.props.admin) {
      route = (
        <Switch>
          <Route path = "/admin" component = {AdminLayout} />
          <Redirect to = "/admin/home" />
        </Switch>
      )
     } else {
      route = (
        <Switch>
          <Route path = "/userHome" component = {UserLayout} />
          <Redirect to = "/userHome" />
        </Switch>
      )
     }
    }

    return (
      <BrowserRouter>
        <div className="App">
          {route}
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    admin: state.auth.admin
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
