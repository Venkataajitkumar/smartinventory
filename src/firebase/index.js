import firebase from 'firebase/app';
import 'firebase/storage';

var config = {
    apiKey: "AIzaSyDkCWJYv91UK74I9x4t-d4WoF6TVXN58dk",
    authDomain: "react-smart-inventory.firebaseapp.com",
    databaseURL: "https://react-smart-inventory.firebaseio.com",
    projectId: "react-smart-inventory",
    storageBucket: "react-smart-inventory.appspot.com",
    messagingSenderId: "919330636777"
};

firebase.initializeApp(config);

const storage = firebase.storage();
export {
    storage, firebase as default
};