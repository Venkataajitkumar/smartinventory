import axios from 'axios';

const instance = axios.create({
    baseURL: "https://react-smart-inventory.firebaseio.com/"
});

export default instance;