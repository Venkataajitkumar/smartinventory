import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Aux from '../../../hoc/Auxiliary/Auxiliary';
import NavigationItems from '../../../components/Navigations/AdminNavigationItems/NavigationItems';
import adminHome from '../../../containers/Admin/Home/AdminHome';
import Products from '../../../containers/Admin/Products/ProductView';
import Notifications from '../../../containers/Admin/Notifications/Notifications';
import Payment from '../../../containers/Admin/Payment/Payment';
import Chats from '../../../containers/Admin/Chats/Chats';
import classes from './AdminLayout.module.css';

class AdminLayout extends Component {

    render() {
        return (
            <Aux>
                <div className = {classes.Navigations}>
                    <NavigationItems />
                </div>
                <div className = {classes.Body}>
                    <Switch>
                        <Route path = "/admin/payment" component = {Payment}/>
                        <Route path = "/admin/notifications" component = {Notifications}/>
                        <Route path = "/admin/chats" component = {Chats} />
                        <Route path = "/admin/home/products" component = {Products} />
                        <Route path = "/admin/home" exact component = {adminHome} />
                    </Switch>
                </div>
            </Aux>
        );
    }
}

export default AdminLayout;