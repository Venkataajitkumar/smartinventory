import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Aux from '../../Auxiliary/Auxiliary';
import NavigationItems from '../../../components/Navigations/UserNavigationItems/NavigationItems';
import UserHome from '../../../containers/User/Home/UserHome';
import UserHistory from '../../../containers/User/History/UserHistory';
import userNotifications from '../../../containers/User/Notifications/Notifications';
import UserPayment from '../../../containers/User/Payment/UserPayment';
import UserProfile from '../../../containers/User/Profile/Profile';
import classes from './UserLayout.module.css';

class UserLayout extends Component {
    render() {
        return (
            <Aux>
                <div className = {classes.Navigations}>
                    <NavigationItems />
                </div>
                <div className = {classes.Body}>
                    <Switch>
                        <Route path = {this.props.match.path + '/payment'} component = {UserPayment}/>
                        <Route path = {this.props.match.path + '/history'} component = {UserHistory}/>
                        <Route path = {this.props.match.path + '/profile'} component = {UserProfile}/>
                        <Route path = {this.props.match.path + '/notification'} component = {userNotifications}/>
                        <Route path = {'/userHome'} component = {UserHome}/>
                    </Switch>
                </div>
            </Aux>
        );
    }
}

export default UserLayout;