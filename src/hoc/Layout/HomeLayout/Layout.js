import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import classes from './Layout.module.css';
import Navigation from '../../../components/Navigations/HomeNavigation/NavigationItems';
import Home from '../../../containers/Authentication/Home/Home';
import SignUp from '../../../containers/Authentication/User/Signup';

class Layout extends Component {
    render() {
        return (
            <div className = {classes.Layout}>
                <div className = {classes.Navigation}>
                    <p>Smart Inventory</p>
                    <Navigation />
                </div>
                <Route path={this.props.match.path + 'signup'}  component = {SignUp} />
                <Home />
            </div>
        );
    }
}

export default Layout;