import React from 'react';

import classes from './Button.module.css';

const button = (props) => (
    <button 
        style = {props.style}
        className = {[classes.Button, classes.btnType].join(' ')}
        disabled = {props.disabled}
        onClick = {props.clicked}>{props.children}</button>
);

export default button;