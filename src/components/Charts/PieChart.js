import React from 'react';
import { Pie } from 'react-chartjs-2';
import { connect } from 'react-redux';

const pieChart = (props) => {

    const data =props.dataNeedToAdd;

    const chartData = {
        labels: ['SmartPhones', 'Laptops', 'Personal Computers', 'Television', 'Gaming', 'Others'],
        datasets:[
        {
            data:data,
            backgroundColor:[
            'rgba(255, 99, 132, 0.6)',
            'rgba(54, 162, 235, 0.6)',
            'rgba(255, 206, 86, 0.6)',
            'rgba(75, 192, 192, 0.6)',
            'rgba(153, 102, 255, 0.6)',
            'rgba(250, 159, 64, 0.6)',
            'rgba(250, 99, 132, 0.6)'
            ],
            borderWidth:1,
            borderColor:'#777',
            hoverBorderWidth:2,
            hoverBorderColor:'#000'
        }]
    };

    return (
        <div>
            <Pie
                data={chartData}
                options={{
                    title:{
                        display:true,
                        text:'Total products added',
                        fontSize:25
                    },
                    legend:{
                        display:true,
                        position:"left"
                    }
                }}/>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        dataNeedToAdd: state.admin.data
    }
}

export default connect(mapStateToProps)(pieChart);