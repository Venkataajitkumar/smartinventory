import React from 'react'; 
import { Link } from 'react-router-dom';
import classes from './NavigationItem.module.css';

const navigationItem = (props) => (
    <Link 
        className = {[classes.NavigationItem, classes[props.type]].join(' ')}
        to = {props.link}>{props.children}</Link>
);

export default navigationItem;