import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.module.css';
import Button from '../../UI/Button/Button';
import * as actions from '../../../store/actions/index';

class NavigationItems extends Component {

    state = {
        userName: 'userName'
    }

    componentWillMount() {
        let userName = null;
        this.props.onUserDataFetched();
        this.props.onInitData();
        this.props.onUserFetch();
        for (let data of this.props.userData) {
            if(data.userId === this.props.userId) {
                userName = data.name;
                break;
            }
        }
        this.setState({
            userName: userName
        })
    }

    render() {
        return (
            <div className = {classes.NavigationItems}>
                <NavigationItem type = "Main" link = "/userHome">Welcome, {this.state.userName}</NavigationItem>
                <NavigationItem link = "/userHome/history">History</NavigationItem>
                <NavigationItem link = "/userHome/notification">Notifications</NavigationItem>
                <NavigationItem link = "/userHome/payment">Payment</NavigationItem>
                <NavigationItem link = "/userHome/profile">Profile</NavigationItem>
                <NavigationItem type="SelfAlign" link = "/"><Button style={{
                    margin: "0px",
                    padding: "0px",
                    fontWeight: "500"
                }}
                clicked = {this.props.onAuthLogout}>LOGOUT</Button></NavigationItem>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        userData: state.userData.userData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuthLogout: () => dispatch(actions.authLogout()),
        onUserDataFetched: () => dispatch(actions.userDataFetched()),
        onInitData: () => dispatch(actions.initData()),
        onUserFetch: () => dispatch(actions.initUserData())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavigationItems);