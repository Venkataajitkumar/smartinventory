import React, { Component } from 'react';

import classes from './NavigationItems.module.css';
import Modal from '../../UI/Modal/Modal';
import AdminLogin from '../../../containers/Authentication/Admin/Login';
import UserLogin from '../../../containers/Authentication/User/Login/Login';
import Button from '../../UI/Button/Button';

class NavigationItems extends Component {

    state = {
        userShown: false,
        adminShown: false
    }

    onAdminClicked = () => {
        this.setState({ adminShown: true });
    }

    onUserClicked = () => {
        this.setState({ userShown: true });
    }

    onAdminCancelled = () => {
        this.setState({ adminShown: false });
    }

    onUserCancelled = () => {
        this.setState({ userShown: false });
    }

    render() {
        return (
            <div>
                <div className = {classes.Login}>
                    <Modal 
                        show={this.state.adminShown} 
                        clicked={this.onAdminCancelled}
                        style = {{
                            width: "40%",
                            left: "30%",
                            height: "350px",
                            paddingTop: "100px",
                            borderRadius: "10px"
                        }}>
                        <AdminLogin />
                    </Modal>
                </div>
                <div  className = {classes.Login}>
                    <Modal 
                        show = {this.state.userShown} 
                        clicked={this.onUserCancelled}
                        style = {{
                            width: "40%",
                            left: "30%",
                            height: "400px",
                            paddingTop: "100px",
                            borderRadius: "10px"
                        }}>
                        <UserLogin clicked = {this.onUserCancelled}/>
                    </Modal>
                </div>
                <div className = {classes.Navigations}>
                    <Button clicked = {this.onAdminClicked}>ADMIN</Button>
                    <Button clicked = {this.onUserClicked}>USER</Button>
                </div>
            </div>
        )
    }
}

export default NavigationItems;
