import React from 'react'; 
import { Link } from 'react-router-dom';

import classes from './NavigationItem.module.css';

const navigationItem = (props) => (
    <Link 
        to = {props.link}
        className = {[classes.NavigationItem, classes[props.type]].join(' ')}>{props.children}</Link>
);

export default navigationItem;