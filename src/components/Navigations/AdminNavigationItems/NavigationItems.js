import React, { Component } from 'react';
import { connect } from 'react-redux';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.module.css';
import Button from '../../UI/Button/Button';
import * as actions from '../../../store/actions/index';

class NavigationItems extends Component {
    render() {
        return (
            <div className = {classes.NavigationItems}>
                <NavigationItem type = "Main" link = "/admin/home">Welcome, ADMIN</NavigationItem>
                <NavigationItem link = "/admin/notifications">Notifications</NavigationItem>
                <NavigationItem link = "/admin/chats">Chats</NavigationItem>
                <NavigationItem link ="/admin/payment">Payment</NavigationItem>
                <NavigationItem type = "SelfAlign" link = "/"><Button style={{
                    margin: "0px",
                    padding: "0px",
                    fontWeight: "500"
                }}
                clicked = {this.props.onAuthLogout}>LOGOUT</Button></NavigationItem>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuthLogout: () => dispatch(actions.authLogout())
    }
}

export default connect(null, mapDispatchToProps)(NavigationItems);