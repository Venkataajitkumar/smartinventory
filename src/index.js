import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import adminReducer from './store/reducers/adminReducer';
import userReducer from './store/reducers/userReducer';
import authReducer from './store/reducers/authReducer';
import userDataReducer from './store/reducers/userDataReducer';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
    admin: adminReducer,
    user: userReducer,
    auth: authReducer,
    userData: userDataReducer
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
